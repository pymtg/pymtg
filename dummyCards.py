mountain = {'cost' : {'uncoloroed':0}, 'name' : 'mountain', 'supertype' : 'Land', 'subtype' : 'Mountain', 'flavortext' : 'boooring land', 'abilities' : 'none', 'producesMana' : 'red'}
forest = {'cost' : {'uncoloroed':0}, 'name' : 'forest', 'supertype' : 'Land', 'subtype' : 'Forest', 'flavortext' : 'boooring land', 'abilities' : 'none', 'producesMana' : 'green'}
plains = {'cost' : {'uncoloroed':0}, 'name' : 'plains', 'supertype' : 'Land', 'subtype' : 'Plains', 'flavortext' : 'boooring land', 'abilities' : 'none', 'producesMana' : 'white'}
swamp = {'cost' : {'uncoloroed':0}, 'name' : 'swamp', 'supertype' : 'Land', 'subtype' : 'Swamp', 'flavortext' : 'boooring land', 'abilities' : 'none', 'producesMana' : 'black'}
island = {'cost' : {'uncoloroed':0}, 'name' : 'island', 'supertype' : 'Land', 'subtype' : 'Island', 'flavortext' : 'boooring land', 'abilities' : 'none', 'producesMana' : 'blue'}

birdsOfParadise = {'cost' : {"green":1}, 'name' : 'bird of paradise', 'supertype' : 'creature', 'subtype' : 'bird','flavortext' : 'its A GODAMN BIRD', 'power' : 1,'toughness' : 1,'abilities' : 'mana'}
phyrexianWalker = {'cost' : {"uncolored":3}, 'name' : 'phyrexian walker', 'supertype' : 'artifact-creature', 'subtype' : 'phyrexian', 'flavortext' : 'ITS COMING RIGHT AT US!', 'power' : 3,'toughness' : 3,'abilities' : 'affinity'}
wallOfSpears = {'cost' : {"white":3}, 'name' : 'wall of spears', 'supertype' : 'artifact-creature','subtype' : 'wall', 'flavortext' : 'wall no hit back', 'power' : 1,'toughness' : 5,'abilities' : 'firstStrike'}

wilowisp = {'cost' : {"black":2}, 'name' : 'wil-o-wisp', 'supertype' : 'creature','subtype' : 'shade', 'flavortext' : ' i got nothin', 'power' : 0,'toughness' : 1,'abilities' : 'regenerate'}
hypnoticSpecter = {'cost' : {'black':2},  'name' : 'hypnotic spectre', 'supertype' : 'creature','subtype' : 'shade', 'flavortext' : 'hypnotic!', 'power' : 2,'toughness' : 2,'abilities' : 'discardOnStrike'}
ragingGoblin = {'cost' : {'red':1,'blue':1}, 'name' : 'raging goblin', 'supertype' : 'creature','subtype' : 'goblin', 'flavortext' : 'stupid goblins', 'power' : 1,'toughness' : 1,'abilities' : 'haste'}

redTeam = [birdsOfParadise, phyrexianWalker, wallOfSpears]
blueTeam = [wilowisp, hypnoticSpecter,ragingGoblin]

cards = {'mountain': mountain,\
	 'forest': forest,\
	 'plains': plains,\
	 'swamp': swamp,\
	 'island': island,\
	 'birdsOfParadise': birdsOfParadise,\
	 'phyrexianWalker': phyrexianWalker,\
	 'wallOfSpears': wallOfSpears,\
	 'wilowisp': wilowisp,\
	 'hypnoticSpecter': hypnoticSpecter}