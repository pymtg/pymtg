#having spells check for other classes and inherit there
#instead of having creatures inherit from spells

#target, for instants enchantments and abilities
#comes into play tapped is an ability
class Creature():
#	def __init__(self, owner, cost, name, supertype, basetype, subtype, flavortext, power, toughness, abilities):
	#	Spell.__init__(self, owner, cost, name, supertype, basetype, subtype, flavortext)
	def __init__(self, cardDict):
		self.power = cardDict.get('power')
		self.toughness = cardDict.get('toughness')
		self.abilities = cardDict.get('abilities')
		self.damageCounters = 0
		self.summoningsick = 1
		self.regenerate = 0

		self.attacking = list()
		self.blocking = list()

	def showPower(self):
		print(self.power)
	def showToughness(self):
		print(self.toughness)
	def showAbilities(self):
		print(self.abilities)
	def showDamage(self):
		print(self.damageCounters)

	def showStatus(self): #power and toughness, minus damage counters and whatnot
		print(self.name + str(self.power) + "/" + str(self.toughness - self.damageCounters))

	def placeDamageCounter(self, damage):
		self.damageCounters += damage

	def block(defenders):		#I dont think this should be here
		for defender in defenders:
			attacker.damageCounters = attacker.damageCounters + defender.power

	def regenerate(self):
		self.regenerate = 1	#this flag will be checked before a creature is sent to graveyard
		print(self.owner.name + "'s" + self.name + ' will regenerate.\n')

	def convertedManaCost(self):
		convertedMana = 0
		for manaType in self.manaCost:
			convertedMana += self.manaCost[manaType]
		return convertedMana
						
	def expireSummonSickness(self):
		self.summoningsick = 0
		print(self.owner + "'s" + self.name + "'s summoning sickness expires.." + '\n')