import os
from classPlayer import *
from classSpells import *

gnounc = Player(name = "gnounc", deckPath = os.getcwd() + "\decks\dummy.dck", deckName = "pestilence")#change pestilence to variable
htmlgecko = Player(name = "htmlgecko", deckPath = os.getcwd() + "\decks\dummy.dck", deckName = "pestilence") #change pestilence to variable
#need to create some sort of masking system, so an enchantment that increases handsize
#stops working when destroyed or nullified
#
#perhaps just add a global active effects table
#which points to the cards causing them
#and check every turn whether that card has been removed from play
#include a flag that says "removing from play does not remove effect"

def enforceHandSize(player):				
	#ask player which card to discard, then pass that card to discard() as an argument
	print(player.name + ' discards any cards over ' + str(player.handSize) + '\n')
	while player.checkHandSize() > player.handSize:
		player.discard(player.inHand[0])###DEBUG - player should choose card to discard.
		
def checkSummoningSickness(player):
	for card in player.inPlay:
		if card.supertype == "creature":		#CHECK MAY NOT WORK AS IS
			card.expireSummonSickness()


#you cannot attack with a creature or 
#play its activated abilities with t or q in their costs 
#unless it has been continuously under your control since 
#the beginning of your most recent turn. 
#(That's "most recent turn," not just "untap step," 
#to prevent weird things happening with cards like Sands of Time)


def resetCreatureDamage(playerlist):
	for eachPlayer in playerlist:
		for eachCreature in eachPlayer.inPlay:
			eachCreature.damageCounters = 0
			print(eachCreature.name + "damage counters: " + str(eachCreature.damageCounters))
	print("all creature damage has been reset.." + '\n')

def resetCreatureTargets(playerlist):
	for eachPlayer in playerlist:
		for eachCreature in eachPlayer.inPlay:
			eachCreature.blocking = list()
#put in print to prove it
	print("all creature blocking targets have been reset.." + '\n')


def allowNonInstants(player):
	print('for this phase only, ' + player.name + ' may play non instants' + '\n')

def globalUntap(player):
	print(player.name + ' untaps..')
	for permanant in player.inPlay:
		permanant.untap()
	#objects should be in charge 
	#of whether or not they are allowed to untap, 
	#and refuse this command if they cannot.

	#no, ive reversed my decision here.
	#the board needs to know before the object knows
	#so we can gray out the card if its not a valid
	#target

	#every action must pass through boardEval() and be evaluated
	#for validity and for how it will act
	#after that is done the board will be re evaluated and updated
	#and cards in your hand you cant play will become grayed out etc.

def confirmPass(segment):#hook to button.

	#debug
	if 1 == 1:
		return# so i dont have to confirm priorities and turns during debug...its slooow

	if segment == 1:
		passPriorityYN = input ('Pass Priority?')	

	if segment == 2:
		passPriorityYN = input ('End Step?')	

	if segment == 3:
		passPriorityYN = input ('End Phase?')

	if segment == 4:
		passPriorityYN = input ('End Turn?')


def passPriority(player):
#need to ensure priority passes in the correct order
#currently it passes arbitrarily
#i think making playerlist a tuple would fix that.
#since lists are unordered

	confirmPass(1)
	player.hasPriority = 0
	print(player.name + ' passes priority..\n'...'')

	for players in playerlist:
		if players != player:
			players.hasPriorty = 1
			confirmPass(players)

	print("priority returns to " + player.name + '\n')

def finishStep(player):
	confirmPass(2)

def finishPhase(player):
	confirmPass(3)

def finishTurn(player):
	player.turn = player.turn + 1
	confirmPass(4)

def checkManaBurn():
	for player in playerlist:
		player.health = player.health - len(player.manaPool)
		print(player.name + ' burns for: ' + str(len(player.manaPool)))
		print(str(player.health) + '\n')
		player.manaPool = []

def declareAttackers(player):
	#INSERT CHOOSE CARD SHIT HERE

	targetPlayer = input('\nchoose Player to attack: ')
	attackingCreature = input('\nchoose Creature to attack with: ')
	
	#INSERT LOOP SO YOU CAN CHOOSE MULTIPLE ATTACKERS

	player.declareAttackers()

def declareBlockers(player):
	#INSERT CHOOSE CARD SHIT HERE

	blockingCreature = input('\nchoose Creature to block with: ')

	#INSERT LOOP SO YOU CAN CHOOSE MULTIPLE BLOCKERS

	player.declareBlockers()

def beginCombat(player):
	print('combat begins:' + '\n')

def resolveCombat():
	for player in playerlist:
		for eachCard in player.inPlay:
			if eachCard.toughness - eachCard.damageCounters < 1:
				player.bury(eachCard)

	print('\n' + 'combat resolves..')
	print('creatures with toughness less than one are buried' + '\n')

def playerDraw(player):
	player.draw(1)

def nextPhase(step, player):
	if (step ==  'untapStep'):
		print('-=untap=-')
		checkSummoningSickness(player)
		globalUntap(player)

	if (step ==  'upkeepStep'):
		print('-=upkeep=-')

	if (step ==  'drawStep'):
		print('-=draw=-')
		playerDraw(player)

	if (step ==  'mainStep'):
		print('-=main=-')
		allowNonInstants(player)

	if (step ==  'beginningStep'):
		print('-=beginning=-')

	if (step ==  'attackingStep'):
		print('-=attack=-')
		declareAttackers(player)

	if (step ==  'blockingStep'):
		print('-=block=-')
		declareBlockers(player)

	if (step ==  'combatStep'):
		print('-=combat=-')
		beginCombat(player)

	if (step ==  'postCombatStep'):
		print('-=post Combat=-')
		resolveCombat()

	if (step ==  'endStep'):
		print('-=end=-')

	if (step ==  'cleanupStep'):
		print('-=cleanup=-')
		enforceHandSize(player)
		resetCreatureDamage(playerlist)
		resetCreatureTargets(playerlist)

beginningPhase = ['untapStep', 'upkeepStep', 'drawStep']
mainPhase = ['mainStep']
combatPhase = ['beginningStep', 'attackingStep', 'blockingStep', 'combatStep', 'postCombatStep']
endPhase = ['endStep', 'cleanupStep']

turn = [beginningPhase, mainPhase, combatPhase, mainPhase, endPhase]


def main():
	maincount = 6
	while maincount > 0:
		for player in playerlist:
			print('\n\n')
			print("-=" + player.name + ' begins turn' + '=-\n')
			if player.turn == 0:
				player.draw(7)
			else:
				player.draw()
			print(player.showInHand())
			for phase in turn:
				for step in phase:
					print(' ')
					nextPhase(step, player)
					if step == ('untapStep' or 'cleanupStep'):
						finishStep(player)
						continue
					else:
						passPriority(player)
					checkManaBurn()
					finishStep(player)
				finishPhase(player)	#hook to button so player can manually end his phase
			finishTurn(player)		#same as above...maybe write a check so player doesnt have to endphase then endturn since theyre really the same thing
			print(player.name + ': ')
			player.showInHand()
			print('turn: ' + str(player.turn))
			print('discarded: ' + str(player.discardNum))				#or just do endTurn() = {endPhase()}
			maincount = maincount - 1				#i did that last one
main()




#implement:

#play 1 land per turn only (responsibility of boardEvail(), will be implemented much much later)
#tap for mana
#casting cost DONE (untested)
#cast(go to play) DONE (untested)

#declare attackers & blockers
#lose health
#trample
#regenerate

#terror
#unholy strength
#dark ritual
#enchantment going to graveyard when creature becomes invalid target (dies)