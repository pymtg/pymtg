#interfaces for player functions are the responsibility of the board
#from dummyCards import *
import random
from classDeck import *
from dummyCards import *
from classSpells import *
playerlist = list()

class Player():
	def __init__(self, name, deckPath, deckName):
		self.health = 20
		self.deckName = ''
		self.deckPath = ''
		self.handSize = 7
		self.name = name

		self.playedLand = 0
		self.hasPriority = 0

		self.attackers = list()
		self.blockers = list()

		self.inLibrary = list()
		self.inHand = list()
		self.inPlay = list()
		self.inGrave = list()
		self.inExile = list()
		self.manaPool = list()
#		self.manaPool = {'uncolored': 1, 'red': 1, 'blue': 1, 'black': 1, 'green': 1, 'white': 1}
	#	self.landsCast = 0
		self.turn = 0
		self.discardNum = 0

		print(self.name + ' chose ' + deckName)

		f = open(deckPath,'r')
		f = f.readlines()

		for name in f:
			if name.strip() in cards.keys():
				self.inLibrary.append(Spell(cards.get(name.strip())))

			else:
				print(name.strip() + " not in deck")

		random.shuffle(self.inLibrary)		

		playerlist.append(self)

	def shuffleLibrary(self):
		self.showLibrary()
		print("\nshuffling...\n")
		random.shuffle(self.inLibrary)
		self.showLibrary()
		
	def search(self, card):
		if card in self.library:
			return self.library.pop(self.library.index.card)
			print('---card found---\n' + card.name)

	def putOnTop(self, card):
		#send it a list of cards and they should all go on top, so 
		#no need to rework for multiple cards
		print("\n" + card.name)
		self.library.insert(0, card)
		
	def viewTop(self, x):
		print ('\n---top ' + str(x) + ' cards---')
		return myDeck[0:x]

	print("\n")

	def showHealth(self):
		print(self.health)

	def showInLibrary(self):
		print ('\n--- library ---')

		for eachCard in self.inLibrary:
			print(eachCard.name)

		print("\n")

	def showInHand(self):
		print ('\n--- hand ---')

		for eachCard in self.inHand:
			print(eachCard.name)

		print("\n")

	def showInPlay(self):
		print ('\n--- board ---')

		for eachCard in self.inPlay:
			print(eachCard.name)

		print("\n")

	def showInGrave(self):
		print ('\n--- grave ---')

		for eachCard in self.inGrave:
			print(eachCard.name)

		print("\n")

	def showInExile(self):
		print ('\n--- exile ---')

		for eachCard in self.inExile:
			print(eachCard.name)

		print("\n")
			
	def draw(self, quantity = 1):
		for x in range(quantity):
			self.inHand.append(self.inLibrary.pop())
		
		if quantity > 1:
			print(self.name + ' draws ' + str(quantity) + ' cards:' + '\n')
		else:
			print(self.name +  " draws a card.")

	def discard(self, card):       
		self.inGrave.append(card)
		self.inHand.remove(card)
		self.discardNum = self.discardNum + 1
		print(self.name + "'s " + card.name + ' was discarded..\n')

	def checkHandSize(self):
		size = 0
		for card in self.inHand:
			size += 1
		return size

	def convertedManaPool(self):
		convertedMana = 0
		for manaType in self.manaPool:
			convertedMana += self.manaPool[manaType]
		return convertedMana
						
	def cast(self, card):
		if self.convertedManaPool() < card.convertedManaCost():		#need to create convertedManaPool and convertedManaCost
			print ('insufficient mana')
			return

		for manaType in card.cost:
			if manaType != uncolored:
				if self.manaPool[manaType] < card.cost[manaType]:
					print('insufficient ' + manaType + ' mana')
					break					#if at any point, insufficient mana of a manatype, stop

	
		#if you have enough of EVERY mana type, subtract it from manapool
		
		for manaType in card.cost:
			if manaType != uncolored:
				manaPool[manaType] = manaPool[manaType]- manaType.value()

		for mana in range(card.cost[uncolored]):	#not sure this is right. supposed to say for each uncolored mana,
			pass
			#ask which mana color to use

			#cast card
		print(self.name + " casts " + card.name + '\n')

		self.inPlay.append(card)
		self.inHand.remove(card)

		print (self.manaPool)

###################################################

	def declareAttackers(targetPlayer, cardList):
		print(self.name + " attacks with:\n " )

		for card in cardList:
			card.attacking.append(targetPlayer)
			self.attackers.append(card)

			print(card.name)

#main() and gui is responsible for asking how many attackers to block and when you're done
	def declareBlockers(targetAttacker, card):
		card.blocking.append(targetAttacker)	##remember to clear this field in cleanup phase
		self.blockers.append(card)

		print(self.name + " blocks " + targetAttacker.name + " with " + card.name)


