from classLands import *
from classCreatures import *

class Spell(Creature):
	def __init__(self, cardDict):

		self.owner = cardDict.get('owner')	#need to initialize this somehow from the player who creates it
		self.cost = cardDict.get('cost')
		self.name = cardDict.get('name')
		self.supertype = cardDict.get('supertype')
		self.subtype = cardDict.get('subtype')
		self.flavortext = cardDict.get('flavortext')
		self.tapped = 0	

#		print(self.name + " is type " + self.supertype)

		if self.supertype == 'Land':
			Land.__init__(self, cardDict)

		if self.supertype == 'creature':
			Creature.__init__(self, cardDict)

######### These Classes do not exist yet
#
#		if self.supertype == 'enchantment':
#			Enchantment.__init__(self, cardDict)

#		if self.supertype == 'artifact':
#			Artifact.__init__(self, cardDict)

#		if self.supertype == 'instant':
#			Instant.__init__(self, cardDict)

#		if self.supertype == 'sorcery':
#			Sorcery.__init__(self, cardDict)

#
#
##WORKS!!!!

	def showName(self):
		print(self.name)
	def showCost(self):
		print(self.cost)
	def showTapped(self):
		print(self.tapped)
	def showFlavor(self):
		print(self.flavortext)
	def target(self, target):
		print(target)
	def showSuperType(self):
		print(self.superType)
	def showType(self):
		print(self.type)
	def showSubType(self):
		print(self.subType)

	def tap(self):
		self.tapped = 1
		print('tapped')
	def untap(self):
		self.tapped = 0
		print('untapped')
	def toggleTap(self):
		if self.tapped == 1:
			self.tapped = 0
		else:
			self.tapped = 1

	def convertedManaCost(self):
		convertedMana = 0
		for manaType in self.manaCost:
			convertedMana += self.manaCost[manaType]
		return convertedMana

#remember to remove card from play or hand or wherever it was when it buried or exiled
	def bury(self):#put regen check here? yes, and have BoardEval() check the flag before it sends it to graveyard
		self.owner.inGrave.append(self)
		print(self.owner.name + "'s " + self.name + ' was destroyed..\n')

	def exile(self):
		self.owner.inExile.append(self)
		print(self.owner.name + "'s " + self.name + ' was exiled..\n')

