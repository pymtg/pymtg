from classDeck import *
#from classPlayer import *
from tkinter import *
import os
from glob import glob

print ('mtgWindows loaded')

def viewHand():
	viewHandInit()


def viewHandInit():
	global viewHandWindow
	global handContents
	viewHandWindow = Tk()
	viewHandWindow.title("hand")

	scrollbar = Scrollbar(viewHandWindow)
	scrollbar.pack(side=RIGHT, fill=Y)
	
	handContents = Listbox(viewHandWindow, yscrollcommand=scrollbar.set, activestyle='none', height=20, width=30)
	scrollbar.config(command=handContents.yview)
	handContents.pack()
	
def deckWindowInit():
	print("Your deck path is: " + os.getcwd())

	global root
	global decks
	global decklistbox
	root = Tk()
	root.title("Choose a Deck")
#	root.wm_iconbitmap(default="C:\Program Files\MagicTG\Icon.ico")

	decklistbox = Listbox(root, activestyle='none')
	decklistbox.pack()

	decks = list()

	for deck in glob(os.getcwd() + "\*.dck"):
		decks.append(deck)

	for i in decks:
		decklistbox.insert(END,os.path.splitext(os.path.basename(i))[0])


	decklistbox.selection_set(first=0)
	decklistbox.focus_set() 


def getSelectedDeck():

	chosen = decks[int(decklistbox.curselection()[0])]
	return chosen

def handButtonInit():
	buttonViewHand = Button(root, text="View Hand", command=viewHand)
	buttonViewHand.pack()

def deckButtonInit():
	buttonChoose = Button(root, text="Choose Deck", command=readDeckFile)
	buttonChoose.pack()


def readDeckFile():
	deckFile = open(getSelectedDeck(), 'r')
	myDeck = deckFile.readlines()
	myDeck = [newline.rstrip() for newline in myDeck]


	myDeckWindow = Tk()
	myDeckWindow.title(myDeck[0])
	
	scrollbar = Scrollbar(myDeckWindow)
	scrollbar.pack(side=RIGHT, fill=Y)

	deckContents = Listbox(myDeckWindow, yscrollcommand=scrollbar.set, activestyle='none', height=20, width=30)
	scrollbar.config(command=deckContents.yview)

	deckContents.pack()
	for i in myDeck[1:]:
		deckContents.insert('end', i)

	deckContents.focus_set()

deckWindowInit()
deckButtonInit()
handButtonInit()

gnounc = Deck(os.getcwd())
gnounc.shuffleLibrary()
gnounc.draw(7)
#gnounc.showLibrary()
#gnounc.viewTop(3)

mainloop()
