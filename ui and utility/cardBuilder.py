import pickle
import os

emptyCard = {'name': '',
	      'color': '',
	      'maintype': '',
	      'subtype': '',
	      'toughness': '',
	      'strength': '',
	      'expansionName': '',
	      'manaCost' : '',
	      'providesMana': []
	      }


def selectionWindow():
	print("*** Welcome to the card builder. ***")
	print("Please make a selection:")
	print("1. Create a new card")
	print("2. View or modify an existing card")
	print("3. Quit")
	selection = input('# ')

	if selection == '1':
		createNewCard()

	elif selection == '2':
		viewExistingCard()

	elif selection == '3':
		exit()

	else:
		print("\n\nYou have made an invalid selection.\n\n")


def createNewCard():
	print("Let's make a new card!")
	tempNewCard = dict(emptyCard)
	name = input('Enter a name for this card: ')
	tempNewCard['name'] = str(name)

	while 1:

		for i in tempNewCard.items():
			print(i)
		print("\nPlease make a selection: ")
		print("1. Change a value")
		print("2. Save card")
		print("3. Go back")
		selection = input('# ')


		if selection == '1':
			valueName = input('Value name: ')

			if valueName in tempNewCard.keys():
				valueSetting = input('Value setting: ')
				tempNewCard[str(valueName)] = valueSetting
				print("Card updated!")

			else:
				print('\n\nThat\'s not a valid name!\n\n')
		

		elif selection == '2':
			cardsObject = dict()

			try:
				cardsFile = open('cards.clist', 'rb')
				cardsObject = pickle.load(cardsFile)
				cardsFile.close() # Might be unnecessary
				cardsObject[tempNewCard['name']] = tempNewCard
				cardsFile = open('cards.clist', 'wb')
				pickle.dump(cardsObject, cardsFile)
				cardsFile.close()
				print("\n\nCard saved!\n\n")

			except(IOError):
				print("\n\nCouldn't save card! The file doesn't exist.\n\n")

			except:
				cardsObject[tempNewCard['name']] = tempNewCard
				cardsFile = open('cards.clist', 'wb')
				pickle.dump(cardsObject, cardsFile)
				cardsFile.close()
				print("\n\nThere was an error, but I was able to save the card anyways!\n\n")

		elif selection == '3':
			selectionWindow()


		else:
			print("\n\nYou have made an invalid selection\n\n")


def viewExistingCard():
	cardFile = open('cards.clist','rb')
	unpackedCardFile = pickle.load(cardFile)
	for i in unpackedCardFile.keys():
		print(i)

	showThisCard = input('Please choose a card: ')

	if showThisCard in unpackedCardFile.keys():
		print(unpackedCardFile[showThisCard])

selectionWindow()
