from classDeck import *
from tkinter import *
import os
from glob import glob

print ('mtgWindows loaded')
	
def deckWindowInit():	#create decklist box window and populate the list of decks
	print("Your deck path is: " + os.getcwd())
	global root
	global decklist
	global decklistbox
	root = Tk()
	root.title("Choose a Deck")
	root.wm_iconbitmap(default="C:\Program Files\MagicTG\Icon.ico")

	decklistbox = Listbox(root, activestyle='none')

	decklistbox.bind("<<ListboxSelect>>", updateDeck)
	decklistbox.pack()

	decklist = list()

	for deck in glob(os.getcwd() + "\*.dck"):
		decklist.append(deck)

	for i in decklist:
		decklistbox.insert(END,os.path.splitext(os.path.basename(i))[0])

	decklistbox.selection_set(first=0)
	decklistbox.focus_set() 

	global deckWindow
	global deckContents

	deckWindow = Tk()
	deckWindow.title(decklist[0])
	
	scrollbar = Scrollbar(deckWindow)
	scrollbar.pack(side=RIGHT, fill=Y)

	deckContents = Listbox(deckWindow, yscrollcommand=scrollbar.set, activestyle='none', height=20, width=30)
	scrollbar.config(command=deckContents.yview)

	deckContents.pack()

	updateDeck(decklist[0])

def updateDeck(deck):
	chosenDeck = decklist[int(decklistbox.curselection()[0])]

	f = open(chosenDeck, 'r')
	deck = f.readlines()
	deck = [newline.rstrip() for newline in deck]

	deckContents.delete(0, END)	#clear decklistbox

	for i in deck[1:]:			#(re)populate decklistbox
		deckContents.insert('end', i)

	deckWindow.title(deck[0])

def viewHandInit():
	global viewHandWindow
	global handContents
	viewHandWindow = Tk()
	viewHandWindow.title("hand")

	scrollbar = Scrollbar(viewHandWindow)
	scrollbar.pack(side=RIGHT, fill=Y)
	
	handContents = Listbox(viewHandWindow, yscrollcommand=scrollbar.set, activestyle='none', height=20, width=30)
	scrollbar.config(command=handContents.yview)
	handContents.pack()

deckWindowInit()

#gnounc =
#gnounc.shuffleLibrary()
#gnounc.draw(7)
#gnounc.showLibrary()
#gnounc.viewTop(3)

mainloop()