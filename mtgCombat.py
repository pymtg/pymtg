from classPlayer import *
from classCreatures import *
from dummyCards import *

#add check for just 1 blocker, so you dont have to distribute damage counters
#when there are no options

#add check for no blockers at all, resulting in health being taken from the player
#add check for firststrike
#add check for trample

#probably need to put those checks in their own file(s)

#placeholder for player.health
health = 20



def battle(attacker, defenders):

	print ('\nBATTLE!\n')


	attacker.showStatus()
	print(' ATTACKS!.. and is blocked by: ')
	

	for defender in defenders:
		defender.showStatus

	attacker.block(defenders)

#----Place Damage Counters----

	print('\n---place damage counter---\n')

	for defender in defenders:	# if checkValidTarget(defender) (not actually sure where or how this check should be proformed)
		print("%d " % (i,) + defender.name)
		i += 1
		
	choice = input("Choose a card: ")

	defenders[int(choice) - 1].addDamageCounters(attacker.power)
	
	#cardname: 2/1   (name followed by power and toughness minus damage counters)
	defenders[int(choice) - 1].showStatus()


#here because at present there IS no player.play
#will be superseded by resolveCombat() when player.play is imported

	if attacker.toughness - attacker.damageCounters < 1:
		attacker.owner.bury(attacker)						#dont know if this is enough to get attacker.owner to point to an actual player 
															#in order to use the player method "bury"				
	for defender in defenders:
		if defender.toughness - defender.damageCounters < 1:
			defender.owner.bury(attacker)

battle(sandWyrm, redTeam)