class Land():
	def __init__(self, cardDict):

		self.abilities = cardDict.get('abilities')
		self.producesMana = cardDict.get('producesMana')

	def tapForMana(self):
		self.tapped = 1
		self.owner.manaPool.append(self.producesMana)